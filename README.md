# trbsvc

A ringbuffer tracelog for go programs

## Description

Adds a sizeable ringbuffer for logging. By default 100 messages are kept in memory, and the oldest messages are overwritten. Useful in web applications to quickly examine the last events. 

### Features

- works as middleware with the go standard library http server

### Built with

- go
- Love

## Getting started

### Prerequisites

None

### Install

Add `gitlab.com/cratermoon/trbsvc` to your go.mod

### Configure

None

### Usage

```go
        rb := trb.NewRingTrace()
        logger := log.New(trb.RingWriter{rb}, "trbscv: ", log.Lshortfile)
        logger.Print("Hello, World!")
```

See the [examples directory](examples/)

### Troubleshooting


## Back matter

### Legal disclaimer

Usage of this tool for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state, and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program.

### Acknowledgements

Thanks to all who helped inspire this template.

### See also

- [TestPoint in Practice](http://c2.com/doc/TestPoint/TestPointInPractice.pdf)

### To-do

- [ ] Implement [labstack echo](https://github.com/labstack/echo) middleware

### License

This project is licensed under the [GNU General Public License v3.0](LICENSE.md).
