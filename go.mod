module gitlab.com/cratermoon/trbsvc

go 1.14

require (
	github.com/segmentio/ksuid v1.0.3
	github.com/stretchr/testify v1.7.0
)
