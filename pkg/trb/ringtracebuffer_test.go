package trb_test

import (
	"bytes"
	"io"
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cratermoon/trbsvc/pkg/trb"
)

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()"

var randsrc = rand.NewSource(time.Now().UnixNano())

func randString(n uint) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[randsrc.Int63()%int64(len(letters))]
	}
	return string(b)
}

func TestEmpty(t *testing.T) {
	rb := trb.NewRingTrace()
	assert.Equal(t, uint(0), rb.Count(), "Should be at zero to start")
}

func TestSize(t *testing.T) {
	assert := assert.New(t)
	rb := trb.NewRingTraceOfSize(3)
	assert.Equal(uint(0), rb.Count(), "Should be at zero to start")
	rb.Append("Hello, World!")
	assert.Equal(uint(1), rb.Count(), "Should be 1 after a single append")
	v := randString(17)
	rb.Append(v)
	assert.Equal(v, rb.Last().Message(), "Wrong value")
	assert.Equal(uint(2), rb.Count(), "Should be 2 after a second append")
	v = randString(17)
	rb.Append(v)
	v = randString(17)
	rb.Append(v)
	assert.Equal(uint(3), rb.Count(), "Should be 3 after two more appends")
}

func TestClear(t *testing.T) {
	assert := assert.New(t)
	rb := trb.NewRingTraceOfSize(3)
	rb.Append("Hello, World!")
	assert.Equal(rb.Count(), uint(1), "Should be 1 after a single append")
	v := randString(17)
	rb.Append(v)
	assert.Equal(uint(2), rb.Count(), "Should be 2 after a second append")
	rb.Clear()
	assert.Equal(uint(0), rb.Count(), "Should be at zero to start")
}

func TestAppend(t *testing.T) {
	rb := trb.NewRingTrace()
	rb.Append("Hello, World!")
	assert.Equal(t, rb.Count(), uint(1), "Should be 1 after a single append")
}

func TestLast(t *testing.T) {
	rb := trb.NewRingTrace()
	v := randString(17)
	rb.Append(v)
	assert.Equal(t, v, rb.Last().Message(), "Wrong value")
}

func TestLastWhenEmpty(t *testing.T) {
	rb := trb.NewRingTrace()
	assert.Equal(t, "", rb.Last().Message(), "Wrong value")
}

func TestFormat(t *testing.T) {
	msg := "Test message"
	tEvent := trb.NewTraceEvent(msg)
	assert.Contains(t, tEvent.Format(), msg, "incorrect format")
}

func TestDump(t *testing.T) {
	msg := "Test message"
	rb := trb.NewRingTrace()
	rb.Append(msg)
	// here we do some tricky stuff to capture stdout
	// https://stackoverflow.com/questions/10473800/in-go-how-do-i-capture-stdout-of-a-function-into-a-string
	old := os.Stdout // keep backup of the real stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	rb.Dump()

	outC := make(chan string)
	// copy the output in a separate goroutine so printing can't block indefinitely
	go func() {
		var buf bytes.Buffer
		io.Copy(&buf, r)
		outC <- buf.String()
	}()

	// back to normal state
	w.Close()
	os.Stdout = old // restoring the real stdout
	out := <-outC

	assert.Contains(t, out, msg, "dump should contain the error message")

}

func TestEntries(t *testing.T) {
	rb := trb.NewRingTrace()
	msg := randString(17)
	rb.Append(msg)
	entries := rb.Entries()
	assert.Equal(t, len(entries), 1, "wrong number of entries")
	assert.Contains(t, entries[0], msg, "first entry should contain the formatted event with test message")
}

func TestWrite(t *testing.T) {
	rb := trb.NewRingWriter()
	msg := randString(17)
	n, err := rb.Write([]byte(msg))
	assert.Nil(t, err, "should not cause error")
	assert.Equal(t, n, len(msg), "Write() returned wrong count")
	assert.Equal(t, rb.Count(), uint(1), "Should be 1 after a single Write")
}
