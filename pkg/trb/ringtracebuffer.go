package trb

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/segmentio/ksuid"
)

const defaultBufferSize = 100

type TraceEvent struct {
	message   string
	timestamp time.Time
	id        ksuid.KSUID
}

func NewTraceEvent(msg string) TraceEvent {
	return TraceEvent{
		message:   msg,
		timestamp: time.Now(),
		id:        ksuid.New(),
	}
}

func (e TraceEvent) Message() string {
	return e.message
}

func (e TraceEvent) Format() string {
	return fmt.Sprintf("%s (%s): %s", e.timestamp.Format(time.StampMilli), e.id, e.message)
}

type RingTrace struct {
	mux        sync.Mutex
	ringbuffer []TraceEvent
	bufsize    uint
	bufpos     uint
	entrycount uint
}

type RingWriter struct {
	*RingTrace
}

func NewRingWriter() RingWriter {
	r := RingTrace{bufsize: defaultBufferSize}
	r.ringbuffer = make([]TraceEvent, r.bufsize)
	return RingWriter{&r}
}
func NewRingTrace() *RingTrace {
	r := RingTrace{bufsize: defaultBufferSize}
	r.ringbuffer = make([]TraceEvent, r.bufsize)
	return &r
}

func NewRingTraceOfSize(size uint) *RingTrace {
	r := RingTrace{bufsize: defaultBufferSize}
	r.bufsize = size
	r.ringbuffer = make([]TraceEvent, r.bufsize)
	return &r
}

func (r *RingTrace) Append(s string) {
	r.mux.Lock()
	evt := TraceEvent{s, time.Now(), ksuid.New()}
	r.ringbuffer[r.bufpos] = evt
	r.bufpos = (r.bufpos + 1) % r.bufsize
	// once the buffer is full, the count stops increasing
	if r.entrycount < r.bufsize {
		r.entrycount = r.entrycount + 1
	}
	r.mux.Unlock()
}

func (r *RingTrace) Clear() {
	r.mux.Lock()
	r.ringbuffer = make([]TraceEvent, r.bufsize)
	r.entrycount = 0
	r.bufpos = 1
	r.mux.Unlock()
}

func (r *RingTrace) Count() uint {
	r.mux.Lock()
	defer r.mux.Unlock()
	return r.entrycount
}

func (r *RingTrace) Last() TraceEvent {
	r.mux.Lock()
	defer r.mux.Unlock()
	if r.entrycount == 0 {
		return TraceEvent{}
	}
	return r.ringbuffer[r.bufpos-1%r.bufsize]
}

func (r *RingTrace) Dump() {
	var b strings.Builder
	r.mux.Lock()
	for pos, item := range r.ringbuffer {
		if item != (TraceEvent{}) {
			b.WriteString(fmt.Sprintf("%3d of %d: %s\n", pos+1, r.bufsize, item.Format()))
		}
	}
	r.mux.Unlock()
	fmt.Println(b.String())
}

func (r *RingTrace) Entries() []string {
	e := make([]string, r.entrycount)
	idx := 0
	r.mux.Lock()
	defer r.mux.Unlock()
	for _, item := range r.ringbuffer {
		if item != (TraceEvent{}) {
			e[idx] = fmt.Sprint(item.Format())
			idx = (idx + 1) % int(r.bufsize)
		}
	}
	return e
}

// be an io.Writer
func (r RingWriter) Write(p []byte) (n int, err error) {
	r.Append(string(p))
	return len(p), nil
}
