package middleware_test

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cratermoon/trbsvc/middleware"
)

func TestLog(t *testing.T) {
	testhandler := func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w, "test")
	}
	ts := httptest.NewServer(middleware.Log(http.HandlerFunc(testhandler)))
	defer ts.Close()
	res, err := http.Get(ts.URL)
	assert.NoError(t, err)
	body, err := ioutil.ReadAll(res.Body)
	assert.NotEmptyf(t, body, "body was %s", body)
}

func TestEntries(t *testing.T) {
	w := httptest.NewRecorder()
	req := httptest.NewRequest("GET", "http://example.com/test", nil)
	middleware.Entries(w, req)
	body, _ := ioutil.ReadAll(w.Body)
	assert.NotEmptyf(t, body, "body was %s", body)
}
