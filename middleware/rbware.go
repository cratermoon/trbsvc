package middleware

import (
	"fmt"
	"log"
	"net/http"
	"path"
	"strings"

	"gitlab.com/cratermoon/trbsvc/pkg/trb"
)

var logger *log.Logger
var rw trb.RingWriter

func init() {
	rw = trb.NewRingWriter()
	logger = log.New(rw, "trbscv: ", log.Lshortfile)
}

// Log installs a handler that instruments logging every request
func Log(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger.Printf("%s from %s", r.URL, r.RemoteAddr)
		h.ServeHTTP(w, r)
	})
}


// RingLong installs a HandlerFunc that instruments logging every request
func RingLog(h http.HandlerFunc) http.HandlerFunc {
	return func (w http.ResponseWriter, r *http.Request) {
		logger.Printf("%s from %s", r.URL, r.RemoteAddr)
		h(w,r)
	}
}

// TRBHandler provides routes for viewing the log and log information
func TRBHandler(w http.ResponseWriter, r *http.Request) {
	_, t := shiftPath(r.URL.Path)
	if t == "/last" {
		fmt.Fprint(w, rw.Last().Format())
	} else if t == "/size" {
		fmt.Fprint(w, rw.Count())
	} else {
		for _, e := range rw.Entries() {
			fmt.Fprint(w, e)
		}

	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
