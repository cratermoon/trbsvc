package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/cratermoon/trbsvc/middleware"
)

// An example of a web server using the ring trace middleware.
//

func main() {
	http.Handle("/time", middleware.Log(http.HandlerFunc(timehandler)))
	http.Handle("/", middleware.Log(http.HandlerFunc(handle)))
	http.HandleFunc("/ksuid", middleware.RingLog(timehandler))
	http.HandleFunc("/log/", middleware.TRBHandler)
	log.Fatal(http.ListenAndServe(":8888", nil))
}

func loglasthandler(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprint(w, rb.Last().Format())
}

func logsizehandler(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintln(w, rb.Count())
}

func timehandler(w http.ResponseWriter, r *http.Request) {
	t := time.Now().Format("2006-01-02T15:04:05Z")
	fmt.Fprintf(w, "The time is now %s\n", t)
}

func handle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello,", r.URL)
}
