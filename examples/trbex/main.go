package main

import (
	"fmt"
	"log"
	"gitlab.com/cratermoon/trbsvc/pkg/trb"
)

var suffixes = []string{"st", "nd", "rd"}

func suffix(i int) string {
	if (i == 11 || i == 12 || i == 13) {
		return "th"
	}
	j := i % 10
	if (0 < j && j < 4) {
		return suffixes[j - 1]
	}
	return "th"
}

func main() {
	rb := trb.NewRingTrace()
	logger := log.New(trb.RingWriter{rb}, "trbscv: ", log.Lshortfile)
	logger.Print("Hello, World!")
	rb.Dump()
	for i := 1; i < 102; i++ {
		logger.Print(fmt.Sprintf("Hello for the %d%s time.", i, suffix(i)))
	}
	for _, e := range rb.Entries() {
		fmt.Print(e)
	}
}
